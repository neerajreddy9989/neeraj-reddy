<?php
require_once "logincheck.php";
$curr_room = 'exhibitionhall';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg">
            <img src="assets/images/GIZ Stalls High Resolution.jpg">

            <a href="https://player.vimeo.com/video/543013846" id="exhVideo" class="viewvideo"></a>
           <a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ODZhODJkMGItNmNhNi00MmI1LWE3YjctOThmMDg3YTRmMDI5%40thread.v2/0?context=%7b%22Tid%22%3a%220d7a41cc-1a62-4347-bacd-764a455b5cf2%22%2c%22Oid%22%3a%224cfebf03-3aec-4aa6-88a9-c637329f44e5%22%7d" id="esoz">
                <div class="indicator d-6"></div>
            </a>
            <a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MTZlZDU3MjktYmU2OS00MWQ3LWE5ZjktMmU3NjgyMDQwNzBh%40thread.v2/0?context=%7b%22Tid%22%3a%228fe7bfaa-d54b-413f-bc5b-1035e5d43b58%22%2c%22Oid%22%3a%22242e732a-206a-40b2-98cb-b8067dee1133%22%7d" id="lizolid">
                <div class="indicator d-6"></div>
            </a>
            <a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MTZlZDU3MjktYmU2OS00MWQ3LWE5ZjktMmU3NjgyMDQwNzBh%40thread.v2/0?context=%7b%22Tid%22%3a%228fe7bfaa-d54b-413f-bc5b-1035e5d43b58%22%2c%22Oid%22%3a%22242e732a-206a-40b2-98cb-b8067dee1133%22%7d" id="colsmartA">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/Sustainable Urban Development/SMART-SUT, CIM returning experts.pdf" class="viewpoppdf" id="bondk">
                <div class="indicator d-6"></div>
            </a>
           
            <a href="meeting_room.php" id="bonk2">
                <div class="indicator d-6"></div>
            </a>
            <!-- <a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MTZlZDU3MjktYmU2OS00MWQ3LWE5ZjktMmU3NjgyMDQwNzBh%40thread.v2/0?context=%7b%22Tid%22%3a%228fe7bfaa-d54b-413f-bc5b-1035e5d43b58%22%2c%22Oid%22%3a%22242e732a-206a-40b2-98cb-b8067dee1133%22%7d" id="lizolid">
                <div class="indicator d-6"></div>
            </a> -->
            <a href="assets/resources/Energy/2021-03-19 IGEN.pdf" class="viewpoppdf" id="dubinor">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/Private Sector Development/IGVET 2 PPT- Diaspora.pdf" class="viewpoppdf" id="stiloz">
                <div class="indicator d-6"></div>
            </a>
            <a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YzYzYTdjNjgtYmE5My00OTAyLWE5NDUtN2Q0MmJkNTZhOGY3%40thread.v2/0?context=%7b%22Tid%22%3a%228fe7bfaa-d54b-413f-bc5b-1035e5d43b58%22%2c%22Oid%22%3a%22242e732a-206a-40b2-98cb-b8067dee1133%22%7d" id="dubinor-ointments">
                <div class="indicator d-6"></div>
            </a>
            <!-- <a href="bmdcamp.php" id="bmdcamp">
                <div class="indicator d-6"></div>
            </a>
            <a href="ebovpg.php" id="ebovpg">
                <div class="indicator d-6"></div>
            </a>
            <a href="vkonnecthealth.php" id="vkonnecthealth">
                <div class="indicator d-6"></div>
            </a> -->

        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>
<?php require_once "scripts.php" ?>
<script src="assets/js/image-map.js"></script>
<script>
    ImageMap('img[usemap]', 500);
</script>
<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>